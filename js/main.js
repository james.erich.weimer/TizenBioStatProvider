// refreshRate frpm 1 to 100 Hz
var refreshRate = 100;
var isLocallyStoring = false;
var runNumber = 0;
var hrmFileName = "hrm";
var lin_accFileName = "lin_acc";
var hrmRecoveryFileName = "Recovery_" + hrmFileName;
var lin_accRecoveryFileName = "Recovery_" + lin_accFileName;

var hrmCharCnt = -1;
var linAccCharCnt = -1;

var debug = false;
var providerAppName = "BioStat";
var batteryLevel = 1;
var isCharging = false;
var pedometerStepStatus = "";
var pedometerAccumulativeTotalStepCount = 0;
var stepOffset = 0;

var interval = Math.round(1000 / refreshRate);
var fileWriteBlock = refreshRate;
var restartDuration = 150000;
var isTimedOut = false;
var isRecovery = false;
var isSending = true;
var isSampling = true;
/*
 * function test(){ isTimedOut = true; isRecovery = true; startSensors(); }
 */

function updateRefreshRate(newRate) {
	refreshRate = newRate;
	interval = Math.round(1000 / refreshRate);
	fileWriteBlock = refreshRate;
	hrmSensor = tizen.sensorservice.getDefaultSensor("HRM_RAW");
	hrmSensor.unsetChangeListener();
	hrmSensor.setChangeListener(onGetSuccessCB_HRM, interval);

}

function dataParser(channelId, data) {
	var json = data;
	var obj = JSON.parse(json);
	switch (obj.cmd) {
	case "start":
		var promiseSetUp = new Promise(function(resolve, reject) {
			updateRefreshRate(obj.refreshRate);
			isLocallyStoring = obj.localStorage;
			hrmFileName = obj.hrmPrefix;
			lin_accFileName = obj.linAccPrefix;
			restartDuration = obj.restartDuration;

			isRecovery = false;
			isSending = true;
			cnt = 0;
			runNumber = 0;
			stepOffset = 0;
			pedometerAccumulativeTotalStepCount = 0;
		});

		var promiseStartSensor = function() {

			startSensors();
			createHTML("Refresh rate: " + refreshRate.toString());
			createHTML("Local storage: " + isLocallyStoring);
			createHTML("HRM Prefix: " + hrmFileName);
			createHTML("LinAcc Prefix: " + lin_accFileName);
			createHTML("RestartDuration: " + restartDuration);
		};

		promiseSetUp.then(promiseStartSensor());
		break;
	case "stop":

		stopSensors();

		break;
	case "start_local":
		isRemote = true;
		var promiseSetUp = new Promise(function(resolve, reject) {
			updateRefreshRate(obj.refreshRate);
			isLocallyStoring = obj.localStorage;
			hrmFileName = obj.hrmPrefix;
			lin_accFileName = obj.linAccPrefix;
			restartDuration = obj.restartDuration;

			isRecovery = false;
			isSending = false;
			cnt = 0;
			runNumber = 0;
			stepOffset = 0;
			pedometerAccumulativeTotalStepCount = 0;
		});

		var promiseStartSensor = function() {

			startSensors();
			createHTML("Refresh rate: " + refreshRate.toString());
			createHTML("Local storage: " + isLocallyStoring);
			createHTML("HRM Prefix: " + hrmFileName);
			createHTML("LinAcc Prefix: " + lin_accFileName);
			createHTML("RestartDuration: " + restartDuration);
		};

		promiseSetUp.then(promiseStartSensor());
		break;

	case "stop_local":
		isRemote = false;
		isSending = true;
		sendLocalAndStopSensors();
		break;
	case "changeRefreshRate":
		// Only want to update the values when sensors are running

		if (isSensorRunning) {
			runNumber++;
			updateRefreshRate(obj.refreshRate);
		}
		break;
	case "resume":
		if (isTimedOut) {
			// While timer not ending, check for resume payload
			// If resume payload, break and send all of local temp storage file,
			// delete it, increase runNum, resume sending

			// Upon resume do stuff

			// No need for the sensors to stop from timeOut

			var promiseSetUp = new Promise(function(resolve, reject) {
				isTimedOut = false;
				isRecovery = false;
				isSampling = false;
				createHTML("PromiseSetup");
				readAndSendFile(hrmRecoveryFile, currHrmRecoveryFileName);

				readAndSendFile(lin_accRecoveryFile,
						currLin_accRecoveryFileName);

				// createHTML(recovery1);
				createHTML("PromiseSend");
			});
			var promiseFinish = function() {
				createHTML("Restart Connection Successful");
				runNumber++;
				isSampling = true;
				createHTML("PromiseFinish");
			};

			promiseSetUp.then(promiseFinish());
		}

		break;
	default:
		createHTML("Unknown data received: " + data);
	}
}

function sendLocalAndStopSensors() {
	var promiseSetUp = function() {
		var promise = new Promise(function(resolve, reject) {
			stopSensors();
		});
		return promise;
	};

	var startTransfer = function(success) {
		var promise = new Promise(function(resolve, reject) {

			var startPayLoadObj = new Object();
			startPayLoadObj.cmd = "start_transfer";
			sendPayLoad(startPayLoadObj);
		});
		return promise;

	};

	var preamble = function(success) {

		var promise = new Promise(function(resolve, reject) {
			var preamblePayLoadObj = new Object();
			preamblePayLoadObj.cmd = "preamble";
			preamblePayLoadObj[hrmFile] = hrmCharCnt;
			preamblePayLoadObj[lin_accFile] = linAccCharCnt;
			sendPayLoad(preamblePayLoadObj);
		});
		return promise;
	};

	var sendHrm = function(success) {

		var promise = new Promise(function(resolve, reject) {
			readAndSendFile(hrmFile, currHrmFileName);
		});
		return promise;
	};
	var sendLinAcc = function(success) {

		var promise = new Promise(function(resolve, reject) {
			readAndSendFile(lin_accFile, currLin_accFileName);
		});
		return promise;
	};

	promiseSetUp().then(startTransfer()).then(preamble()).then(sendHrm()).then(
			sendLinAcc());

}

function sendPayLoad(payLoad) {
	console.log("Sending: " + JSON.stringify(payLoad));
	send(JSON.stringify(payLoad) + "\n");
}

function restartConnection() {
	if (!isRemote) {
		createHTML("Attempting to restart connection");
		// Store all of the data to local temp storage with increased runNum

		// Waiting for there to be a resume command so isTimeOut can be false

		runNumber++;
		createFile(hrmRecoveryFileName);
		createFile(lin_accRecoveryFileName);
		isRecovery = true;

		isTimedOut = true;
		setTimeout(function() {
			// Wait for there to be a command parsed, else when time-out,
			// sensors
			// will close
			if (isTimedOut) {
				stopSensors();
				createHTML("Restart Connection Timed Out");
			}
		}, restartDuration);
	} else {
		stopSensors();
	}

}

/* Sensor Functions */

var dir = "documents";

var currHrmFileName = hrmFileName;

var currLin_accFileName = lin_accFileName;

var currHrmRecoveryFileName = hrmRecoveryFileName;
var currLin_accRecoveryFileName = lin_accRecoveryFileName;
var hrmFile = "HRM_FILE";
var lin_accFile = "LIN_ACC_FILE";

var hrmRecoveryFile = "HRM_RECOVERY_FILE";
var lin_accRecoveryFile = "LIN_ACC_RECOVERY_FILE";
var winEvent = null;
var cnt = 0;
var bufferMap = null;

var sensorCapability = true;
var isSensorRunning = false;
var sensorCapabilityMap = new Map();
sensorCapabilityMap.set("HRM_IR",
		"http://tizen.org/feature/sensor.heart_rate_monitor.led_ir");
sensorCapabilityMap.set("HRM_RED",
		"http://tizen.org/feature/sensor.heart_rate_monitor.led_red");
sensorCapabilityMap.set("HRM_G",
		"http://tizen.org/feature/sensor.heart_rate_monitor.led_green");
sensorCapabilityMap.set("LINEAR_ACCELERATION",
		"http://tizen.org/feature/sensor.linear_acceleration");

// Sensor variables
var hrmSensor = null;

// helper functions for getting timestamp info
function getTime() {
	var date = new Date(Date.now());
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	var minutes = "0" + date.getMinutes();
	var seconds = "0" + date.getSeconds();
	var ms = "0" + date.getMilliseconds();
	var time = year + '_' + month + '_' + day + '_' + hours + '_' + minutes.substr(-2) + '_' + seconds.substr(-2)
			+ '_' + ms.substr(-3);

	return time;
}

function getDate() {
	var date = new Date(Date.now());
	var currentYear = date.getFullYear().toString();
	var currentMonth = date.getMonth().toString();
	var currentDate = date.getDate().toString();
	var time = currentYear + "_" + currentMonth + "_" + currentDate;
	return time;
}

// sensor callback action, interval set by HRM sensor
function onGetSuccessCB_HRM(sensorData) {
	if (isSampling) {
		updateView();
		// update battery level
		tizen.systeminfo.getPropertyValue("BATTERY", onBatterySuccessCallback,
				onBatteryErrorCallback);

		// get labels
		var header = new Object();
		header.timestamp = getTime();
		header.battery = batteryLevel;
		header.isCharging = isCharging;
		header.refreshRate = refreshRate;
		header.runNumber = runNumber;
		header.cnt = cnt++;

		/*
		 * var time = "\"timestamp\": " + "\"" + getTime() + "\", " +
		 * "\"battery\": " + batteryLevel + ", \"isCharging\": " + isCharging;
		 * var counter = "\"cnt\": " + cnt++;
		 */

		// create HRM payload
		var hrmPayLoadObj = new Object();
		for ( var k in header) {
			hrmPayLoadObj[k] = header[k];
		}
		hrmPayLoadObj.dataType = "HRM";

		hrmPayLoadObj.intensity = sensorData.lightIntensity;
		hrmPayLoadObj.pedomStatus = pedometerStepStatus;
		hrmPayLoadObj.pedomTotalStep = pedometerAccumulativeTotalStepCount;
		var hrmPayload = JSON.stringify(hrmPayLoadObj) + "\n";

		if (debug) {
			createHTML(hrmPayload);
		}

		// update HTML counter for sensor calls
		var cntDiv = document.getElementById("counter");
		cntDiv.innerHTML = cntDiv.innerHTML.substr(0, 14) + cnt;

		// write HRM data to file and send
		if (!isRecovery) {
			writeToFileAndSend(hrmFile, currHrmFileName, hrmPayload, false);

		} else {
			writeToFileAndSend(hrmRecoveryFile, currHrmRecoveryFileName,
					hrmPayload, false);
		}

		// Update char count for hrm
		if (hrmCharCnt === -1) {
			hrmCharCnt = 0;
		}
		hrmCharCnt += hrmPayload.length;

		// store and send linear acceleration data
		if (winEvent !== null) {

			var x = winEvent.acceleration.x;
			var y = winEvent.acceleration.y;
			var z = winEvent.acceleration.z;

			var linAccPayLoadObj = new Object();
			for ( var k in header) {
				linAccPayLoadObj[k] = header[k];
			}
			linAccPayLoadObj.dataType = "LIN_ACC";
			linAccPayLoadObj.x = x;
			linAccPayLoadObj.y = y;
			linAccPayLoadObj.z = z;
			var linAccPayload = JSON.stringify(linAccPayLoadObj) + "\n";

			if (debug) {
				createHTML(linAccPayload);
			}

			if (!isRecovery) {
				writeToFileAndSend(lin_accFile, currLin_accFileName,
						linAccPayload, false);

			} else {
				writeToFileAndSend(lin_accRecoveryFile,
						currLin_accRecoveryFileName, linAccPayload, false);
			}

			// Update char count for lin acc
			if (linAccCharCnt === -1) {
				linAccCharCnt = 0;
			}
			linAccCharCnt += linAccPayload.length;
		}
	}

}

// sensor callback functions
function onsuccessCB_HRM() {
	console.log("HRM Sensor started successfully.");
	hrmSensor.getHRMRawSensorData(onGetSuccessCB_HRM, onerrorCB_HRM);

}

function onerrorCB_HRM(error) {
	console.log("error occurred with HRM");
}

function onchangedCB_Pedometer(pedometerInfo) {

	if (stepOffset === 0) {
		stepOffset = pedometerInfo.accumulativeTotalStepCount;
	}

	pedometerStepStatus = pedometerInfo.stepStatus;

	pedometerAccumulativeTotalStepCount = pedometerInfo.accumulativeTotalStepCount
			- stepOffset;

	console.log('Step status: ' + pedometerInfo.stepStatus);
	console.log('Speed: ' + pedometerInfo.speed);
	console.log('Walking frequency: ' + pedometerInfo.walkingFrequency);
	console.log("Steps: " + pedometerInfo.accumulativeTotalStepCount);

}

function onBatterySuccessCallback(battery) {
	batteryLevel = battery.level;
	isCharging = battery.isCharging;

}

function onBatteryErrorCallback(error) {
	console.log("An error occurred with the battery: " + error.message);
}

// debugging device sensor capability
function testDeviceSensorCapabilities() {
	console.log("testing deviceSensorCapabilities");
	createHTML("Platform Version: "
			+ tizen.systeminfo.getCapabilities().platformVersion);
	if (debug) {
		var sensors = tizen.sensorservice.getAvailableSensors();
		createHTML('Available sensor: ' + sensors.toString());
	}

	var cntr = 0;
	var len = sensorCapabilityMap.size;
	var iter = sensorCapabilityMap.keys();
	while (cntr < len) {
		var sensorName = iter.next().value;

		try {
			tizen.systeminfo.getCapability(sensorCapabilityMap.get(sensorName));
			// createHTML("Capable sensor: " + sensorName + " | "
			// + sensorCapabilityMap.get(sensorName));
		} catch (err) {
			sensorCapability = false;
			createHTML("Uncapable sensor: " + sensorName + " | "
					+ sensorCapabilityMap.get(sensorName));
		}
		cntr++;
	}
}

// HTML startSensors action
function startSensors() {
	chBackcolor('#196F3D');
	try {

		if (!isSensorRunning) {

			// clear log
			var log = document.getElementById("resultBoard");
			log.innerHTML = "";

			// Reset transfer counts
			hrmCharCnt = -1;
			linAccCharCnt = -1;

			// test device
			testDeviceSensorCapabilities();

			// update batteryLevel before any sending
			tizen.systeminfo.getPropertyValue("BATTERY",
					onBatterySuccessCallback, onBatteryErrorCallback);
			if (sensorCapability) {

				isSensorRunning = true;

				if (!debug) {
					createHTML("Debugging disabled");
				}

				// update HTML counter for sensor calls
				var cntDiv = document.getElementById("counter");
				cntDiv.innerHTML = cntDiv.innerHTML.substr(0, 14) + cnt;
				document.getElementById("file-progress").value = 0;
				document.getElementById("percent").innerHTML = "0%";

				if (isLocallyStoring) {
					// create data file
					createFile(hrmFileName);
					createFile(lin_accFileName);
				}

				// create buffer mapping
				bufferMap = new Map();
				bufferMap.set(hrmFile, "");
				bufferMap.set(lin_accFile, "");
				bufferMap.set(hrmRecoveryFile, "");
				bufferMap.set(lin_accRecoveryFile, "");

				// start sensors

				hrmSensor.start(onsuccessCB_HRM);

				tizen.humanactivitymonitor
						.setAccumulativePedometerListener(onchangedCB_Pedometer);

				createHTML("startSensors success");
			} else {
				createHTML("Sensors uncapable");
				throw new Error("Sensors uncapable");
			}
		} else {
			createHTML("Sensors not stopped");
		}
	} catch (err) {
		createHTML("startSensors exception [" + err.name + "] msg ["
				+ err.message + "]");
	}
}

// HTML stopSensors action
function stopSensors() {
	chBackcolor('#17202A');
	try {
		if (isSensorRunning) {

			if (hrmSensor !== null) {
				hrmSensor.stop();
			}

			// empty the buffers
			writeToFileAndSend(hrmFile, currHrmFileName, "", true);
			writeToFileAndSend(lin_accFile, currLin_accFileName, "", true);

			// unregister pedometer listener
			tizen.humanactivitymonitor.unsetAccumulativePedometerListener();
			createHTML("stopSensors success");
		} else {
			createHTML("Never ran startSensors");
		}

	} catch (err) {
		createHTML("stopSensors exception [" + err.name + "] msg ["
				+ err.message + "]");
	}
	isSensorRunning = false;
}

function updateView() {
	// update progress bar
	var bar = document.getElementById("file-progress");
	bar.max = refreshRate;

	var pc = (cnt + 1) % bar.max;
	bar.value = pc;

	var percent = document.getElementById("percent");
	percent.innerHTML = Math.round((pc * 100) / refreshRate) + "%";
}

function writeToFileAndSend(currFile, fileName, sensorData, blocking) {

	// check if need to write (ie. reached block limit), else store in buffer

	var currFileBuffer = bufferMap.get(currFile) + sensorData;

	if ((cnt % fileWriteBlock === 0) || blocking) {
		if (isSending) {
			send(currFileBuffer);
		}
		if (isLocallyStoring || isRecovery) {
			tizen.filesystem.resolve(dir + "/" + fileName, function(currFile) {
				// open file in append mode
				currFile.openStream("a", onOpenSuccess, null, "UTF-8");

				function onOpenSuccess(fs) {
					// write sensor data to file
					fs.write(currFileBuffer);

					fs.close();
				}
			});
		}

		bufferMap.set(currFile, "");
	} else {
		bufferMap.set(currFile, currFileBuffer);
	}

}

function readAndSendFile(currFileName, fileName) {
	var currFile = "currFile";
	tizen.filesystem.resolve(dir + "/" + fileName, function(currFile) {
		currFile.openStream("r", function(fs) {
			var text = fs.read(currFile.fileSize);
			fs.close();

			var strings = text.split("\n");
			var i;

			for (i = 0; i < strings.length; i++) {

				if (i === 0) {
					var startTextPayLoadObj = new Object();
					startTextPayLoadObj.cmd = "file_start";
					startTextPayLoadObj.file = currFileName;
					sendPayLoad(startTextPayLoadObj);
				}

				var textPayLoadObj = new Object();
				textPayLoadObj.cmd = "file_transfer";

				var s = strings[i];
				if (s !== "") {
					textPayLoadObj[currFileName] = s;
					sendPayLoad(textPayLoadObj);
				}

				if (i === strings.length - 1) {
					var stopTextPayLoadObj = new Object();
					stopTextPayLoadObj.cmd = "file_stop";
					stopTextPayLoadObj.file = currFileName;
					sendPayLoad(stopTextPayLoadObj);
				}
			}

		}, function(e) {
			console.log("Error " + e.message);
		}, "UTF-8");
	});
}

function createFile(fileName) {
	var documentsDir, testFile;
	var newFileName = fileName + "_" + refreshRate + "Hz_" + getDate() + "_"
			+ getTime().toString() + ".txt";
	createHTML("File created:" + newFileName);

	if (fileName === hrmFileName) {
		currHrmFileName = newFileName;
	}

	if (fileName === lin_accFileName) {
		currLin_accFileName = newFileName;
	}

	if (fileName === hrmRecoveryFileName) {
		currHrmRecoveryFileName = newFileName;
	}

	if (fileName === lin_accRecoveryFileName) {
		currLin_accRecoveryFileName = newFileName;
	}
	// get to default document directory
	tizen.filesystem.resolve(dir, function(result) {
		documentsDir = result;
		testFile = documentsDir.createFile(newFileName);
	});

}

function chBackcolor(color) {
	document.body.style.background = color;
}

function exitApp() {
	tizen.application.getCurrentApplication().exit();
}

function onAppBackground() {
	tizen.power.setScreenBrightness(0);
}

function onAppForeground() {
	tizen.power.setScreenBrightness(0.6);
}

window.onload = function() {
	// add eventListener for accelerometer
	window.addEventListener("devicemotion", function(event) {
		winEvent = event;
	}, true);
	

	// Hide popup at start
	document.getElementById("sideBtnPopup").style.display = "none";

	// add eventListener for tizenhwkey
	document.addEventListener('tizenhwkey', function(e) {
		if (e.keyName === "back") {

			try {
				// Toggling the popup
				var popUp = document.getElementById("sideBtnPopup");
				var mainPage = document.getElementById("mainPage");
				var connectionStat = document
						.getElementById("connectionStatus");
				if (mainPage.style.display === "block"
						&& connectionStat.innerHTML === "Disconnected") {
					popUp.style.display = "block";
					mainPage.style.display = "none";
				} else {
					popUp.style.display = "none";
					mainPage.style.display = "block";
				}

			} catch (ignore) {
			}
		}
	});

	// Control app visibility.
	var hidden, visibilityChange;
	if (typeof document.hidden !== "undefined") {
		hidden = "hidden";
		visibilityChange = "visibilitychange";
	} else if (typeof document.webkitHidden !== "undefined") {
		hidden = "webkitHidden";
		visibilityChange = "webkitvisibilitychange";
	}
	onAppForeground();
	function handleVisibilityChange() {
		if (document[hidden]) {
			console.log("Page is now hidden.");
			onAppBackground();
		} else {
			console.log("Page is now visible.");
			onAppForeground();
		}

	}
	document.addEventListener(visibilityChange, handleVisibilityChange, false);

};
